#!/bin/bash -e

#. /usr/share/gitlab-ci-common
if [ -z "$CI" ]; then
    echo "This script should only ever be run in GitLab CI!"
    exit 1
fi

apt_get="apt-get -qy $APT_GET_OPTIONS"

function apt_get_auto_install() {
    $apt_get update
    $apt_get install $@ && apt-mark auto $@
}

# these are all required
! test -z "$CI_PROJECT_URL"
! test -z "$CI_JOB_ID"
! test -z "$CI_COMMIT_SHA"
! test -z "$CI_COMMIT_REF_NAME"

export REPO_PATH="unstable"

if [ "$CI_PROJECT_NAME" = "$TRIG_PROJECT_NAME" ] ; then
    echo "INFO: did not try to build apt repo for $TRIG_PROJECT_NAME (recursive test)" | \
	tee public/README
    exit 0
fi

generate_index() {
  PUBKEY_FILENAME="public-key.asc"
  REPO_URL="${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/${REPO_PATH}"
  REPO_PUBKEY_URL="${REPO_URL}/${PUBKEY_FILENAME}"

  envsubst <<-EOF
<html>
<head><title>apt source for ${CI_PROJECT_PATH}</title></head>
<body>
<ul>
<li>branch <a href="${CI_PROJECT_URL}/commits/${CI_COMMIT_REF_NAME}">${CI_COMMIT_REF_NAME}</a></li>
<li>commit <a href="${CI_PROJECT_URL}/commit/${CI_COMMIT_SHA}">${CI_COMMIT_SHA}</a></li>
<li>pipeline <a href="${CI_PIPELINE_URL}">${CI_PIPELINE_URL}</a></li>
<li>job <a href="${CI_JOB_URL}">${CI_JOB_URL}</a></li>
</ul><br/>
<pre>$ wget -O - <a href="${REPO_PUBKEY_URL}">${REPO_PUBKEY_URL}</a> | sudo apt-key add -
$ echo | sudo tee /etc/apt/sources.list.d/job-${CI_JOB_ID}.list &lt;&lt;EOF
deb <a href="${REPO_URL}">${REPO_URL}</a> ${RELEASE} main
# deb-src <a href="${REPO_URL}">${REPO_URL}</a> ${RELEASE} main
EOF</pre><br/>
<a href="${CI_JOB_URL}/artifacts/browse">Browse all files</a>
</body>
</html>
EOF
}

#if [ "$CI_JOB_NAME" != "pages" ]; then
#    echo "$0 must be run in a job called 'pages'!"
#    exit 1
#fi

#if [ "$CI_JOB_STAGE" != "deploy" ]; then
#    echo "$0 must be run in a job in the 'deploy' stage!"
#    exit 1
#fi

JOB_URL="$CI_PROJECT_URL/-/jobs/$CI_JOB_ID"
COMMIT_URL="$CI_PROJECT_URL/commit/$CI_COMMIT_SHA"
BRANCH_URL="$CI_PROJECT_URL/commits/$CI_COMMIT_REF_NAME"

apt_get_auto_install gnupg1 gpgv1 gnupg

# let's have a gpg key to play with
gpg --gen-key --batch <<-EOF
	%echo Generating Repo key
	# %dry-run
	Key-Type: default
	Name-Real: TEST on salsa.debian.org
	Name-Comment: don't trust these packages in the wild
	Name-Email: phil-salsatestkeys@hands.com
	Expire-Date: 30d
	%no-protection
	# %pubring foo.pub
	# Do a commit here, so that we can later print "done" :-)
	%commit
	%echo done
EOF

ls -lR /root/.gnupg/
KEYID=$(gpg --list-secret-keys | sed -ne '/^ /{s/^  *//p;q}')
gpg --export-secret-keys | gpg1 --import

# build apt repo from freshly built .debs
aptly="aptly"
$aptly repo create "$REPO_PATH" || true
echo "$1: PWD=$PWD"
$aptly repo add "$REPO_PATH" udebs/*.udeb
DEB_BUILD_ARCH=$(dpkg --print-architecture)
$aptly publish repo --gpg-key=$KEYID --distribution "$REPO_PATH" --architectures all,$DEB_BUILD_ARCH "$REPO_PATH" \
    || $aptly publish update --gpg-key=$KEYID --architectures all,$DEB_BUILD_ARCH "$REPO_PATH"

rm -rf public
cp -a ~/.aptly/public public

gpg -a --export $KEYID > public/archive-key.pub

# this is to ease debugging, since directory indices are disabled
cd public
generate_index > index.html
