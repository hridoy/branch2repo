# Debian pipeline for Debian-Installer udebs

Build udebs into a test mini.iso image on every push.

## Introduction

This builds on top of the Salsa CI Team's [pipeline](https://salsa.debian.org/salsa-ci-team/pipeline/).

The idea here is that you create a new branch for developing a feature or fixing
a bug in one or more of the repos that are brought together by debian-installer,
and this will create a repository containing all the resulting udebs for that
branch, which debian-installer can then use to build a mini.iso.

The repository can also be used to provide udebs to the early stage of booting
that mini.iso (although that currently requires that a kludged version of
net-retreiver be among the udebs that are included in the mini.iso, to provide
the ability to deal with mutltiple repositories)

## Basic Use

Create a file `debian/salsa-ci.yml` with the following contents:

```yaml
---
include:
  - https://salsa.debian.org/installer-team/branch2repo/raw/main/trigger_b2r.yml

variables:
  ### uncomment the following for testing udebs that are not built in to the mini.iso
  #BRANCH2REPO_EXTRA_PROJECTS: 'net-retriever:philh:extra-udeb-repo anna:philh:de-dup'

  ### these variables may be useful for getting things to work as you prefer:
  #SALSA_CI_DISABLE_AUTOPKGTEST: 0
  #SALSA_CI_DISABLE_REPROTEST: 0
  #SALSA_CI_DISABLE_LINTIAN: 0
  #SALSA_CI_DISABLE_APTLY: 0
  #BRANCH2REPO_DISABLE_TRIGGER: 0
  #BRANCH2REPO_DISABLE_MAKEISO: 0

```

The variables are up to you, but the above are a good starting place for most
udeb packages.

### BRANCH2REPO_DISABLE_MAKEISO

  This stops the branch2repo job that hands off to debian-installer to build the
  mini.iso (so it still makes the combined repo, so if you have a mini.iso
  that's willing to load from there, that should be enough for testing things
  that get downloaded into the installer).

### BRANCH2REPO_DISABLE_TRIGGER

  This stops the job that includes trigger_b2r.yml from actually triggering
  branch2repo's job, so setting this has the effect of running just the enabled
  bits of the salsa-CI pipeline. This might be useful for udebs that are going
  to be included via BRANCH2REPO_EXTRA_PROJECTS.

### BRANCH2REPO_EXTRA_PROJECTS

  this allows one to specify extra projects that will be bundled into the repo
  being created by branch2repo, even though they have a different branch.

  If you ever add something here and wish you hadn't (for instance adding the
  tweaked net-retriever & anna udebs when there is no need because the udeb you
  are building is part of the mini.iso and doesn't need the extra help) then
  simply removing this setting doesn't help because it will continue to be
  remembered by the saved state -- you need to edit the state file for the
  relevant branch to remove this. One could add some way of fixing this via the
  settings, but it seems like a rare enough occurrence to leave it as a thing
  one fixes by hand.

The `trigger_b2r.yml` file includes the default Debian recipe from the Salsa CI
Team's pipeline, which is why we need to turn off the elements that we don't
need. Of course, if you get a udeb to build without lintian errors, you don't
need to disable that test above.

You can include this only in the branch where you want to do some testing, or
even in a personal fork of the project, and it should still work.

If you want several udebs to be included in a test, just make sure that they
have matching namespaces (i.e. they're all 'debian-installer' repos, or all
under your own user's namespace), and that the branches you are pushing all have
a matching name. That should be enough to ensure that they all get put together
into a new test image and/or are all available from the same repo such that the
test image can download them.

## What does this _pipeline_ provide for my project/package?

For the elements of the pipeline that occur before the _bridge_ stage, refer to the 
[pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) documentation.

### What happens after the bridge

_branch2repo_ gets triggered, which causes it to first remember the repository
and branch that triggered it, and then build a repository out of all the aptly
repos of all the branches that match both by branchname and namespace.

Once it's built that repo, it in turn triggers debian-installer to build a
mini.iso based on 'unstable' with the addition of the udebs from the current
namespace/branch.

Once that's done, it tells [openQA](https://openqa.debian.net/) about the new mini.iso, which
provokes openQA to run tests on it.
